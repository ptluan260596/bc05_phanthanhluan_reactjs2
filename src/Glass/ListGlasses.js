import React, { Component } from "react";
import GlassesItem from "./GlassesItem";
import { dataGlasses } from "./dataGlasses";
export default class ListGlasses extends Component {
  renderGlasses = () => {
    return dataGlasses.map((glass, index) => {
      return (
        <GlassesItem
          handleChangeGlass={this.props.handleChangeGlass}
          key={index}
          img={glass.url}
          name={glass.name}
          price={glass.price}
          desc={glass.desc}
        />
      );
    });
  };
  render() {
    let { listGlasses } = this.props;
    return (
      <div>
        <div className="listGlasses">
          <div className="row">{this.renderGlasses()}</div>
        </div>
      </div>
    );
  }
}
