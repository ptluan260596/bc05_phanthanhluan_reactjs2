import React, { Component } from "react";
import { dataGlasses } from "./dataGlasses";
export default class GlassesItem extends Component {
  render() {
    let { img, name, price, desc } = this.props;
    return (
      <div
        className="col-3 mb-4"
        onClick={() => {
          this.props.handleChangeGlass(img, name, price, desc);
        }}
      >
        <div className="card p-3 glass-item-card">
          <img className="card-img-top" src={img} alt="" />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">Price: {price}$</p>
            <p className="card-text" style={{ fontSize: "12px" }}>
              {desc}
            </p>
          </div>
        </div>
      </div>
    );
  }
}
