import React, { Component } from "react";

export default class Model extends Component {
  render() {
    let { img, name, price, desc } = this.props;
    return (
      <div className="model">
        <div className="card text-white bg-dark">
          <img
            style={{ width: "100%", objectFit: "cover" }}
            src="./glassesImage/model.jpg"
            className="img-model"
            alt=""
          />
          <img
            src={img}
            alt=""
            className="img-glass"
            style={{
              position: "absolute",
              top: "150px",
              left: "51%",
              transform: "translateX(-51%)",
              width: "55%",
              opacity: "70%",
            }}
          />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">Price: {price}$</p>
            <p className="card-text">{desc}</p>
          </div>
        </div>
      </div>
    );
  }
}
