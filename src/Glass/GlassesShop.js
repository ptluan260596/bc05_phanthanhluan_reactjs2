import React, { Component } from "react";
import { dataMovie } from "./dataGlasses";
import Model from "./Model";
import ListGlasses from "./ListGlasses";
export default class GlassesShop extends Component {
  state = {
    imageUrl: "./glassesImage/v1.png",
    name: "GUCCI G8850U",
    price: 30,
    desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.",
  };
  handleChangeGlass = (img, name, price, desc) => {
    this.setState({
      imageUrl: img,
      name: name,
      price: price,
      desc: desc,
    });
  };

  render() {
    let { imageUrl, name, price, desc } = this.state;
    return (
      <div
        className="wrapper"
        style={{
          backgroundImage: "url(./glassesImage/background.jpg",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
          backgroundSize: "cover",
          height: "100vh",
          objectFit: "cover",
        }}
      >
        <h1 className="pt-4 title">Fashion Glass Shop</h1>
        <div className="container-fluid">
          <div className="row pt-4">
            <div className="col-4">
              <Model img={imageUrl} name={name} price={price} desc={desc} />
            </div>
            <div className="col-8">
              <ListGlasses
                handleChangeGlass={this.handleChangeGlass}
                listGlasses={this.dataGlasses}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
